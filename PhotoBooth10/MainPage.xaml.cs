﻿using System;
using System.IO;
using System.Threading.Tasks;
using Windows.Media.Capture;
using Windows.Media.MediaProperties;
using Windows.Storage;
using Windows.System;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;
using Microsoft.OneDrive.Sdk;
using Microsoft.OneDrive.Sdk.Authentication;

namespace PhotoBooth10
{
    public sealed partial class MainPage
    {
        private readonly MediaCapture _captureManager;
        private const int PhotoDelay = 5;
        private int _currentWaitTime;
        private readonly DispatcherTimer _delayTimer;
        private readonly string[] _scopes = { "onedrive.readwrite", "wl.signin" };

        public OneDriveClient OneDriveClient { get; private set; }

        public MainPage()
        {
            InitializeComponent();
            _captureManager = new MediaCapture();

            _currentWaitTime = 5;

            _delayTimer = new DispatcherTimer
            {
                Interval = TimeSpan.FromSeconds(1)
            };
            _delayTimer.Tick += TimerTick;

            Loaded += PageLoaded;
            Window.Current.CoreWindow.KeyUp += HandleKeyUp;
            Window.Current.CoreWindow.PointerPressed += CoreWindow_PointerPressed;
        }


        private void CoreWindow_PointerPressed(CoreWindow sender, PointerEventArgs args)
        {
            Dispatcher?.RunAsync(CoreDispatcherPriority.High, () =>
            {
                Countdown.Text = PhotoDelay.ToString();
            });

            _delayTimer.Start();
        }

        private async void InitializeClient()
        {
            if (OneDriveClient != null)
                return;

            var authProvider = new OnlineIdAuthenticationProvider(_scopes);
            await authProvider.AuthenticateUserAsync();
            OneDriveClient = new OneDriveClient("https://api.onedrive.com/v1.0", authProvider);
            if (OneDriveClient == null)
            {
                throw new Exception("Unable to connect to OneDrive");
            }
        }

        private void TimerTick(object sender, object e)
        {
            _currentWaitTime--;
            if (_currentWaitTime < 0)
                _delayTimer.Stop();

            if (_currentWaitTime == 0)
            {
                _delayTimer.Stop();
                _currentWaitTime = PhotoDelay;
                SaveImage();

                Dispatcher?.RunAsync(CoreDispatcherPriority.High, () =>
                {
                    Countdown.Text = "";
                });

                ThreadPoolTimer.CreateTimer(p => Dispatcher?.RunAsync(CoreDispatcherPriority.High, () =>
                {
                    SavedImage.Visibility = Visibility.Collapsed;
                }), TimeSpan.FromSeconds(3));

                return;
            }

            Dispatcher?.RunAsync(CoreDispatcherPriority.High, () =>
            {
                Countdown.Text = _currentWaitTime.ToString();
            });
        }

        private void HandleKeyUp(CoreWindow sender, KeyEventArgs e)
        {
            if (e.VirtualKey != VirtualKey.Number1)
                return;

            Dispatcher?.RunAsync(CoreDispatcherPriority.High, () =>
            {
                Countdown.Text = PhotoDelay.ToString();
            });

            _delayTimer.Start();
        }

        private async void PageLoaded(object sender, RoutedEventArgs e)
        {
            await _captureManager.InitializeAsync();
            CapturePreview.Source = _captureManager;

            await _captureManager.StartPreviewAsync();
            await SetResolution();

            InitializeClient();
        }

        private async void SaveImage()
        {
            string fileName = string.Format("Photo {0}.jpg", DateTime.Now.ToString("dd hh-mm-ss"));

            var photoStorageFile = await KnownFolders.PicturesLibrary.CreateFileAsync(fileName, CreationCollisionOption.GenerateUniqueName);
            var properties = ImageEncodingProperties.CreateJpeg();

            await _captureManager.CapturePhotoToStorageFileAsync(properties, photoStorageFile);
            using (var capturedPhotoStream = await photoStorageFile.OpenStreamForReadAsync())
            {
                // Show preview to indicate that the photo worked
                var image = new BitmapImage();
                image.SetSource(capturedPhotoStream.AsRandomAccessStream());
                SavedImage.Source = image;
                SavedImage.Visibility = Visibility.Visible;

                // Reset the location of the stream so it can be read again.
                capturedPhotoStream.Position = 0;

                // Upload to OneDrive
                await OneDriveClient.Drive.Root
                    .ItemWithPath("/PhotoBooth/" + fileName)
                    .Content
                    .Request()
                    .PutAsync<Item>(capturedPhotoStream);
            }
        }

        public async Task SetResolution()
        {
            var resolutions = _captureManager.VideoDeviceController.GetAvailableMediaStreamProperties(MediaStreamType.VideoPreview);
            int maxResolution = 0;
            int indexMaxResolution = 0;

            if (resolutions.Count >= 1)
            {
                for (int i = 0; i < resolutions.Count; i++)
                {
                    var vp = (VideoEncodingProperties)resolutions[i];

                    if (vp.Width > maxResolution && vp.Subtype.Equals("YUY2"))
                    {
                        indexMaxResolution = i;
                        maxResolution = (int)vp.Width;
                    }
                }

                await _captureManager.VideoDeviceController.SetMediaStreamPropertiesAsync(MediaStreamType.VideoPreview, resolutions[indexMaxResolution]);
            }
        }
    }
}
